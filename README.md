# doctrine/doctrine-migrations-bundle

[doctrine/doctrine-migrations-bundle](https://packagist.org/packages/doctrine/doctrine-migrations-bundle)
![Packagist Downloads](https://img.shields.io/packagist/dm/doctrine/doctrine-migrations-bundle)

# Unofficial documentation
* [*Why & How to use Doctrine Migrations Rollup?*
  ](https://odolbeau.fr/blog/doctrine-migrations-rollup.html)
  Cause keeping your old migrations is just useless.
  2020-05 Olivier Dolbeau
